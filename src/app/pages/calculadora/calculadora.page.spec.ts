import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { CalculadoraPage } from './calculadora.page';

describe('A página calculadora', () => {
  let view;
  let model: CalculadoraPage;
  let fixture: ComponentFixture<CalculadoraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculadoraPage ],
      imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CalculadoraPage);
    model = fixture.componentInstance;
    view = fixture.nativeElement;
    fixture.detectChanges();
  }));

  it('deve ter um título',() => {
    expect(model.titulo).toBeDefined();
  });

  it('deve renderizar o titulo', () =>{
    const result = view.querySelector('.title').textContent;
    expect(result).toEqual('Calculadora');
  });

  it('deve estar com o botão desabilitado', () => {
    expect(model.form.invalid).toBeTrue();
  });

  it('divide com clique no botão', () => {
    model.form.controls.divisor.setValue(4);
    model.form.controls.dividendo.setValue(20);

    const operacao = fixture.debugElement.query(By.css('#operacao'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    const quociente = view.querySelector('#quociente');
    expect(quociente.textContent).toEqual('5');

  });

});
