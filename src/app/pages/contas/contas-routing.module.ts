import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CadastroPage } from './cadastro/cadastro.page';
import { PagarPage } from './pagar/pagar.page';
import { ReceberPage } from './receber/receber.page';
import { RelatorioPage } from './relatorio/relatorio.page';

const routes: Routes = [
  {
    path: '', children:[
      { path: 'pagar', component: PagarPage },
      { path: 'receber', component: PagarPage },
      { path: 'cadastro', component: CadastroPage },
      { path: 'relatorio', component: RelatorioPage },
    ]
  },
  {
    path: 'lista',
    loadChildren: () => import('./lista/lista.module').then( m => m.ListaPageModule)
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PagarPage,
    ReceberPage,
    CadastroPage,
    RelatorioPage
  ]
})
export class ContasRoutingModule { }
