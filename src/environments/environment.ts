// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDq80I7trUUAeaLXo65BBg8vtzapw3sGEg',
    authDomain: 'controle-bonfire.firebaseapp.com',
    databaseURL: 'https://controle-bonfire.firebaseio.com',
    projectId: 'controle-bonfire',
    storageBucket: 'controle-bonfire.appspot.com',
    messagingSenderId: '1062243922992',
    appId: '1:1062243922992:web:9f42137faa2d86bbd169f5',
    measurementId: 'G-WMVCPM187Z'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
